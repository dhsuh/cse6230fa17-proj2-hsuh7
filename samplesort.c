#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include "proj2sorter.h"
#include "proj2sorter_impl.h"

uint64_t cmp(const void * v1, const void * v2){
	return ( *(uint64_t*)a - *(uint64_t*)b);
}

static int ChooseSamplePivot(Proj2Sorter sorter, MPI_Comm comm, size_t numKeysLocal, uint64_t *keys, uint64_t *Buckets)
{
  int err;


}

static int Proj2SorterSort_samplesort(Proj2Sorter sorter, MPI_Comm comm, size_t numKeysLocal, uint64_t *keys)
{
  uint64_t pivot = 0;
  uint64_t *lower_half, *upper_half;
  size_t   lower_size, upper_size;
  size_t   numKeysLocalNew = 0;
  uint64_t *keysNew = NULL;
  int      color;
  int      size, rank;
  int      equivRank;
  MPI_Comm subcomm;
  int      err;
  uint64_t   *Splitter, *AllSplitter, *Buckets, *BucketsLength;


  err = MPI_Comm_size(comm, &size); PROJ2CHK(err);
  err = MPI_Comm_rank(comm, &rank); PROJ2CHK(err);

  /* sort locally up front */
  err = Proj2SorterSortLocal(sorter, numKeysLocal, keys, PROJ2SORT_FORWARD); PROJ2CHK(err);
  if (size == 1) {
    /* base case: nothing to do */
    return 0;
  }

 


  Splitter = (uint64_t *) malloc (sizeof (uint64_t) * (size-1));
  for (int i=0; i< (size-1); i++){
	Splitter[i] = keys[(i+1)*(numKeysLocal/(size-1))];
  }

  AllSplitter = (uint64_t *) malloc (sizeof (uint64_t) * size * (size-1));
  MPI_Gather(Splitter, size-1, MPI_UINT64_T, AllSplitter, size-1, MPI_UINT64_T, 0, comm);
  
  if (rank == 0){
	qsort(AllSplitter, size*(size-1), sizeof(uint64_t), uint64_comp_forward);
	for (int i=0; i<(size-1); i++)
		Splitter[i] = AllSplitter[(size-1)*(i+1)];
  }

  MPI_Bcast(Splitter, size-1, MPI_UINT64_T, 0, comm);
  Buckets = (uint64_t *) malloc (sizeof(uint64_t) * size); //Store indices of pivots per processes  
  BucketsLength = (uint64_t *) malloc (sizeof(uint64_t) * size);

  int data_index = 0;
  for (int index=0; index<size; index++){
	Buckets[index] = data_index;
	BucketsLength[index] = 0;

	while((data_index<(numKeysLocal-1)) && (keys[data_index]<=Splitter[index])){
		BucketsLength[index]++;
		data_index++;
	}
  }
  Buckets[size] = numKeysLocal-1;
  BucketsLength[size] = numKeysLocal-1 - Buckets[size-1];


  uint64_t *recvBucket, *recvBufLen, *recvBuf;
  recvBuf = (uint64_t *) malloc (sizeof(uint64_t) * numKeysLocal); // Does the size need to be bigger????.....
  recvBufLen = (uint64_t *) malloc (sizeof(uint64_t) * size);
  recvBucket = (uint64_t *) malloc (sizeof(uint64_t) * size);
  for(int proc=0; proc<size; proc++){  
	MPI_Gather(BucketsLength[proc], 1, MPI_UINT64_T, recvBufLen,1,MPI_UINT64_T,proc,comm);

	if (rank == proc){
		recvBucket[0]=0;
		for (int i=1; i<size; i++){
			recvBucket[i] = recvBucket[i-1] + recvBufLen[i-1];
		}
	}

	MPI_Gatherv(keys[Buckets[proc]], BucketsLength[proc], MPI_UINT64_T,recvBuf, recvBufLen, recvBucket, MPI_UINT64_T, proc, comm);
  }

  uint64_t *sorteddata, *sendlen, *sendstart, mysendlength = recvBucket[size-1] + recvBufLen[size-1];

  sendlen = (uint64_t *) malloc (sizeof(uint64_t) * size);
  sendstart = (uint64_t *) malloc (sizeof(uint64_t) * size);

  MPI_Gather(&mysendlength,1,MPI_UINT64_T,sendlen,1,MPI_UINT64_T,comm);

  if (rank==0){
	sendstart[0] = 0;
	for(int i=1; i<size; i++){
		sendstart[i] = sendstart[i-1]+sendlen[i-1];
	}
  }


  sorteddata = (uint64_t *) malloc (sizeof(uint64_t) * numKeysLocal);





}
